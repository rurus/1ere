# TP sur les boucles et les structures linéaires


## Incrément

*input* : une liste L de nombres

*output* : une liste dans laquelle le i-ème élément est égal au i-ème élément de
L plus 1.


Par exemple `[1, 2, 3]` deviendra `[2, 3, 4]`.


Depuis un `input` on récupère une chaîne.  Il faut donc utiliser `eval` (cf doc)
pour revenir à une liste :

```python
In [1]: L = "[1, 2, 3]"

In [2]: eval(L)
Out[2]: [1, 2, 3]

In [3]: type(eval(L))
Out[3]: list

In [4]: type(L)
Out[4]: str
```

## Puissance n

*input* un entier `n` et une liste L de nombres

*output* la liste des puissances n-ème des termes de L



## Le retour

Reprenez les TP précédents et  éventuellement améliorez-les grâce à nos nouveaux
outils.


## Inventions

Imaginez un problème et résolvez-le avec nos nouveaux outils.

## Tous égaux

Créez un  programme qui demande  de rentrer  des nombres et  qui écrit à  la fin
'Tous égaux :)'  si tous les nombres sont égaux.  Proposez plusieurs versions et
discutez de leurs divers intérêts et utilités.


## Compte à rebours

La  commande `sleep(n)`  de la  bibliothèque  `time` permet  d'arrêter le  temps
pendant `n` secondes. Utilisez-là pour créer un compte à rebours où chaque ligne
s'affiche à une seconde d'intervalle :

```python
Combien de secondes ? 6
6 @  @  @  @  @  @ 
5 @  @  @  @  @ 
4 @  @  @  @ 
3 @  @  @ 
2 @  @ 
1 @ 
BOUM !
```

On peut aussi mettre  du son...VLC peut être utilisé depuis  un terminal avec la
commande `cvlc`. Deux fichiers `wav` sont dans le répertoire `IMG`. Testez :

```console
$ cvlc --play-and-exit --quiet ../IMG/tick.wav
```

Pour  lancer une  commande depuis  Python, vous  pouvez utiliser  `popen` de  la
bibliothèque `os`

```python
import time
import os

?????

os.popen('cvlc --play-and-exit --quiet ../IMG/explosion.wav)
```



## j suivi 

Que vaut `j` à la fin de l'exécution de ces fragments de code:

```python
j = 0
for ind in range(10):
    j += 1
```


```python
j = 0
for ind in range(2, 10):
    j += 1
```


```python
j = 0
for ind in range(j, 10):
    j += 1
```


```python
j = 0
for ind in range(10):
    j += j
```


```python
j = 0
for ind in range(j):
    j += 1
```

Réfléchissez d'abord, frappez ensuite et enfin expliquez.






## Quel est le problème ?

Tout est dans le titre

```python
xs = []
for i in range(100):
	xs[i] = 2*i
```


## Lecture

Que fait ce programme (imaginez que c'est une question de l'examen écrit...)

```python
xs = [0]*10
for i in range(10):
	xs[i] = 9 - i
for i in range(10):
	xs[i] = xs[xs[i]]
for x in xs:
	print(x)
```

Vérifiez ensuite et expliquez.



## Dessine-moi un carré

À l'aide d'une boucle dessinez un carré rempli d'étoiles:

```
>>> print(carre)

**********
**********
**********
**********
**********
**********
**********
**********
**********
**********
```


Imaginez des options : taille, symbole, forme,...



## Zip

![zip](./IMG/zip.jpeg)

Créez un `zipper` qui prend deux listes et renvoie une liste de longueur la plus
petite des deux et contenant des couples formé de l'élément `i` de la liste 1 et
l'élément i de la liste 2. Par exemple :

```python
Entrez les éléments de la liste 1 séparés par des espaces : 12 32 1 4 67
Entrez les éléments de la liste 2 séparés par des espaces : a b papa c
[('12', 'a'), ('32', 'b'), ('1', 'papa'), ('4', 'c')]
```


## Upside down

Demandez un mot et renvoyez-le écrit à l'envers (comme `rev` en bash):

```console
Entrez votre mot : Bonjour
À l'endroit : Bonjour
À l'envers  : ruojnoB

Entrez votre mot : kayak
À l'endroit : kayak
À l'envers : kayak
C'est un palindrome !
```




## Poker

Créez un programme qui  demande un entier `n` et qui affiche  `n` mains de Poker
	(5 cartes) d'un jeu mélangé. On pourra regarder l'aide de `random.sample`.


```console
Combien de mains (moins de 10) voulez-vous afficher ? : 10
D♦ 6♦ 5♠ 2♦ 10♥

10♦ 8♣ 7♠ V♥ 2♠

8♥ D♠ 4♥ V♦ 3♥

6♥ V♣ 7♦ 1♠ 9♠

1♦ R♥ 8♦ V♠ 3♦

R♦ 9♦ 9♥ 8♠ 2♣

2♥ 6♠ 6♣ 1♣ D♣

R♣ 3♠ D♥ 1♥ R♠

5♣ 4♣ 9♣ 7♥ 3♣

10♠ 5♦ 10♣ 4♦ 4♠
```

Défi : quelle est la main gagnante ?

## $`\pi`$-statistiques 

Une bibliothèque de  Python nous donne autant de chiffre  de l'écriture décimale
de $`\pi`$ que l'on souhaite.

```python
>>> from mpmath import mp

>>> mp.dps = 200

>>> print(str(mp.pi))
3.141592653589793238462643383279502884197169399375105820974944592307816
40628620899862803482534211706798214808651328230664709384460955058223172
5359408128481117450284102701938521105559644622948954930382

>>> len(str(mp.pi)) 
200
```

On voudrait  obtenir un tableau des  fréquences d'apparition des chiffres  0, 1,
2,..., 9 dans les 100 000 premières décimales de $`\pi`$ sous forme d'une liste.




## Boucles imbriquées (Nested loops)


Expliquez :

```python
In [32]: s = ''                                                          

In [33]: for i in range(5,10): 
    ...:     for j in range(10): 
    ...:         s += f"{i*j}  " 
    ...:     s += "\n" 
    ...:                                                                 

In [34]: print(s)                                                        
0  5  10  15  20  25  30  35  40  45  
0  6  12  18  24  30  36  42  48  54  
0  7  14  21  28  35  42  49  56  63  
0  8  16  24  32  40  48  56  64  72  
0  9  18  27  36  45  54  63  72  81  
```



## Krampouz-dogs imbriquées

Une krampouz-dog contient  ou non une saucisse (pour  les végétariens), contient
ou non une  krampouz (pour les Vendéens),  contient ou non de  la moutarde (pour
les enfants), contient ou non du ketchup (pour les parents), contient ou non des
oignons.

On peut voir la conception d'une  krampouz-dog ainsi (vous terminerez l'arbre en
faisant figurer le Ketchup et les oignons...):

```mermaid
graph TD;
C[Choix] --Avec-->S[Saucisse]
C[Choix] --Sans-->PS[No Saucisse]

S[Saucisse] --Avec-->K1[Krampouz]
S[Saucisse] --Sans-->PK1[No Krampouz]
PS[No Saucisse] --Avec-->K2[Krampouz]
PS[No Saucisse] --Sans-->PK2[No Krampouz]
K1[Krampouz] --Avec-->M1[Moutarde]
K1[Krampouz] --Sans-->PM1[No Moutarde]
PK1[No Krampouz] --Avec-->M2[Moutarde]
PK1[No Krampouz] --Sans-->PM2[No Moutarde]
K2[Krampouz] --Avec-->M3[Moutarde]
K2[Krampouz] --Sans-->PM3[No Moutarde]
PK2[No Krampouz] --Avec-->M4[Moutarde]
PK2[No Krampouz] --Sans-->PM4[No Moutarde]
```


Chaque ingrédient correspond à un certain nombre de calories. Un(e) geek étant très
à  cheval sur  la  diététique,  on donne  les  calories  correspondant à  chaque
ingrédient:

* saucisse : 140
* krampouz : 120
* moutarde : 20
* ketchup : 80
* oignon : 40


Faites figurer chaque combinaison possible avec les calories qui correspondent.

Vous pourrez obtenir par exemple quelque chose comme ça :

```console
	Sa	Kz	Mo	Ke	Og	Calories
1	0	0	0	0	0	0
2	0	0	0	0	1	40
3	0	0	0	1	0	80
4	0	0	0	1	1	120
5	0	0	1	0	0	20
6	0	0	1	0	1	60
7	0	0	1	1	0	100
8	0	0	1	1	1	140
9	0	1	0	0	0	120
10	0	1	0	0	1	160
11	0	1	0	1	0	200
12	0	1	0	1	1	240
13	0	1	1	0	0	140
14	0	1	1	0	1	180
15	0	1	1	1	0	220
16	0	1	1	1	1	260
17	1	0	0	0	0	140
18	1	0	0	0	1	180
19	1	0	0	1	0	220
20	1	0	0	1	1	260
21	1	0	1	0	0	160
22	1	0	1	0	1	200
23	1	0	1	1	0	240
24	1	0	1	1	1	280
25	1	1	0	0	0	260
26	1	1	0	0	1	300
27	1	1	0	1	0	340
28	1	1	0	1	1	380
29	1	1	1	0	0	280
30	1	1	1	0	1	320
31	1	1	1	1	0	360
32	1	1	1	1	1	400
```

On peut utiliser au moins 5 boucles imbriquées...ou moins.




## Pendu

Fabriquez un jeu du pendu qui pourrait se dérouler ainsi :

```console
Mot à deviner : _ _ _ _ _ _ _ 

Choisissez une lettre : a
Pas de chance, recommencez

Mot à deviner : _ _ _ _ _ _ _ 

Choisissez une lettre : e
Bon choix

Mot à deviner : _ _ _ _ _ e _ 

Choisissez une lettre : i
Bon choix

Mot à deviner : _ _ _ i _ e _ 

Choisissez une lettre : u
Pas de chance, recommencez

Mot à deviner : _ _ _ i _ e _ 

Choisissez une lettre : p
Pas de chance, recommencez

Mot à deviner : _ _ _ i _ e _ 

Choisissez une lettre : d
Bon choix

Bon choix

Mot à deviner : d _ _ i d e _ 

Choisissez une lettre : r
Bon choix

Mot à deviner : d _ _ i d e r 

Choisissez une lettre : r
r a déjà été choisie

Mot à deviner : d _ _ i d e r 

Choisissez une lettre : é
Bon choix

Mot à deviner : d é _ i d e r 

Choisissez une lettre : c
Bon choix

Gagné !

Le mot était : DÉCIDER
```

On peut ensuite imaginer des variantes : limiter le nombre d'essais, dessiner un
pendu, utiliser `easygui`, faire jouer l'ordinateur, etc.

Dans le répertoire `Python` du dépôt Gitlab se trouve un fichier avec une liste de mots que vous
pouvez utiliser en l'important sous la forme :

```python
fic = open('mots_frequents.txt', 'r').read()
```

Vous pourrez aussi vous renseigner sur la fonction `choice` de la bibliothèque `random`.




## La balle qui reboondit 

Voici un  code assez  sommaire qui permet  de faire rebondir  une balle  sur les
parois d'un  mur. À vous de  le transformer, de  le compléter, pour en  faire un
jeu, une application,...

```python
#!/usr/bin/env python
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt

d = 1 # figure entre -d et d en abscisses et ordonnées
fig, ax = plt.subplots()
plt.axis([-d, d, -d, d])

# Valeurs de départ de la position et de la vitesse de la balle
x = 0.48
y = -0.17
vx = 0.039
vy = 0.017

# rayon de la balle : 
rayon = d/50
rbd = 0 # nb de rebonds avant l'arrêt

while rbd < 10:
    # la balle rebondit sur les bords
    if abs(x + vx) > d - rayon: 
        vx = -vx
        rbd += 1
    if abs(y + vy) > d - rayon:
        vy = -vy
        rbd += 1
    # ou avance 
    x += vx
    y += vy
    # on dessine la balle 
    cercle = plt.Circle((x, y), radius = rayon, fill = False) # dessine une série de ronds d'aire 1600 
    ax.add_artist(cercle) 
    # on s'arrête entre chaque tracé de cercle
    plt.pause(0.01)
# on montre le tout
plt.show()
```


