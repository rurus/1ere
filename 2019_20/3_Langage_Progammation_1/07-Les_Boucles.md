# Les Boucles


## Boucles conditionnelles

*L'humain pense, la machine répète*

Supposons que l'on veuille afficher une série de "Bonjour" numérotés :

```python
"C'est mon bonjour numéro 1"
"C'est mon bonjour numéro 2"
"C'est mon bonjour numéro 3"
"C'est mon bonjour numéro 4"
```

On écrit :

```python
no = 1                                                          

while no < 5: 
    print( f"C'est mon bonjour numéro {no}" ) 
    no = no + 1 
```

La syntaxe est :

```
while <expression booléenne>:
	faire ceci
	faire cela
faire autre chose quand l'expression booléenne est devenue fausse
```


En fait, cela revient écrire :

```python
no = 1

if no < 5:
    print( f"C'est mon bonjour numéro {no}" ) 
    no = no + 1 
	
if no < 5:
    print( f"C'est mon bonjour numéro {no}" ) 
    no = no + 1 
	
if no < 5:
    print( f"C'est mon bonjour numéro {no}" ) 
    no = no + 1 
	
if no < 5:
    print( f"C'est mon bonjour numéro {no}" ) 
    no = no + 1 
	
....
```

mais qui s'arrêterait lorsque l'expression booléenne devient fausse.


### Incrément

L'expression `no = no  + 1` est bien sûr une affectation et  non pas une égalité
mathématique qui serait absurde.

Comme dans la  plupart des autres langages qui  permettent l'affectation, Python
dispose  d'un *sucre  syntaxique*: `no  +=  1` qui  se décline  avec tout  autre
opérateur `n -= 1`, `n *= 2`, `n /= 2`,...
	

Par exemple **quelle est la première puissance de 2 qui dépasse 10000 ?**


```python
In [39]: n = 1

In [40]: while n < 10000:
    ...:     n *= 2

In [41]: n
Out[41]: 16384

In [42]: n = 1

In [43]: while n < 10000: 
    ...:     n <<= 1

In [44]: n
Out[44]: 16384
```



## Boucles *foreach* : pour chaque élément d'une collection

On peut aussi prendre chaque élément d'une collection et faire quelque
chose avec ces éléments.

Par exemple, on dispose d'une liste d'élèves et on veut leur dire bonjour:

```python
In [49]: classe = {'Joe', 'Max', 'Bill', 'Zoé', 'Isa', 'Zaza'}          

In [50]: for élève in classe: 
    ...:     print(f"Bonjour {élève}") 
    ...:                                                                 
Bonjour Isa
Bonjour Bill
Bonjour Joe
Bonjour Max
Bonjour Zoé
Bonjour Zaza
```

Si l'on veut un peu d'ordre:

```python
In [53]: classe = ['Joe', 'Max', 'Bill', 'Zoé', 'Isa', 'Zaza']           

In [54]: for élève in classe: 
    ...:     print(f"Bonjour {élève}") 
    ...:                                                                 
Bonjour Joe
Bonjour Max
Bonjour Bill
Bonjour Zoé
Bonjour Isa
Bonjour Zaza
```

La syntaxe est donc :

```python
for élément in collection: # pour chaque élément dans la collection
    faire ceci
faire cela # quand on a fini de parcourir TOUS les éléments de la collection
```

## Cas particulier du parcours d'un "range"

Si l'on veut parcourir une  collection de nombres entiers espacés régulièrement,
on peut utiliser  `for indice in range(début,  fin + 1, pas[qui  vaut par défaut
1])`.


Par exemple si on veut sélectionner un  élève sur deux dans la classe en partant
du premier:


```python
In [1]: classe = ['Joe', 'Max', 'Bill', 'Zoé', 'Isa', 'Zaza']                                                                                        

In [2]: for indice in range(0, len(classe), 2): 
   ...:     print(classe[indice]) 
   ...:                                                                                                                                              
Joe
Bill
Isa
```

### DÉFI

On dispose d'une liste `fi = [1, 1, 0, 0, 0, ..., 0]` de longueur 20.

On veut remplacer  tous les termes à  partir du troisième par la  somme des deux
précédents. 

On veut donc obtenir `[1, 1, 2, 3, 5, 8, 13,...]`

Comment faire ?



## Le beurre  et l'argent  du beurre  : obtenir  la valeur  d'un élément  et son rang dans une liste


Comment  mettre  en  majuscule les  lettres  de  rang  pair  d'un mot  écrit  en
minuscules.

On utilisera  la méthode  `upper()` qui  transforme une  lettre en  la majuscule
correspondant:

```python
In [3]: 'a'.upper()                                                                                                                                  
Out[3]: 'A'

In [4]: 'A'.upper()                                                                                                                                  
Out[4]: 'A'

In [5]: '1'.upper()                                                                                                                                  
Out[5]: '1'

In [6]: 'Papa'.upper()                                                                                                                               
Out[6]: 'PAPA'
```


Et  surtout  `enumerate(collection)`  qui  renvoie  la  l'ensemble  des  couples
`(rang, valeur)` pour chaque valeur de la collection.

```python
In [14]: mot = input('Rentrer un mot : ')
Rentrer un mot : anticonstitutionnellement

In [15]: new_mot = ''

In [16]: for rang, lettre in enumerate(mot): 
    ...:     new_mot += lettre.upper() if rang % 2 == 0 else lettre 

In [17]: new_mot
Out[17]: 'AnTiCoNsTiTuTiOnNeLlEmEnT'
```


## Définitions par compréhension


On dispose d'une liste de lettres en minuscules et on veut créer une autre liste
des mêmes lettres en majuscules :

```python
In [21]: ls = ['a', 'b', 'c', 'd']

In [22]: [lettre.upper() for lettre in ls]
Out[22]: ['A', 'B', 'C', 'D']
```

On utilise la construction par compréhension :

```
{ f(x) for x in collection }
# l'ensemble des f(x) pour chaque élément x de la collection
```



### Filtre

On peut utiliser un filtre en  rajoutant une expression booléenne introduite par
`if`. Par exemple, une liste de nombre  étant donnée, on veut fabriquer la liste
des multiples communs de 8 et 6 de cette liste :

```python
>>> [nb for nb in range(200) if nb % 6 == 0 and nb % 8 == 0]
[0, 24, 48, 72, 96, 120, 144, 168, 192]
```



### TODO

Écrire  un  programme  qui demande  un  mot  et  qui  renvoie ce  mot  écrit  en
majuscules.  On pourra  utiliser une  liste  par compréhension  et les  méthodes
`join` et `upper`.

