#!/usr/bin/env python
# coding: utf-8


############################
#
# Z I P P E R
#
##########################


xs1 = (input("\nEntrez les éléments de la liste 1 séparés par des espaces : ")).split()
xs2 = (input("Entrez les éléments de la liste 2 séparés par des espaces : ")).split()

n = min(len(xs1), len(xs2))

cs = []

for i in range(n):
    cs.append((xs1[i], xs2[i]))

print(cs)
