#!/usr/bin/env python
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt

d = 1 # figure entre -d et d en abscisses et ordonnées
fig, ax = plt.subplots()
plt.axis([-d, d, -d, d])

# Valeurs de départ de la position et de la vitesse de la balle
x = 0.48
y = -0.17
vx = 0.039
vy = 0.017

# rayon de la balle : 
rayon = d/40
rbd = 0 # nb de rebonds avant l'arrêt

while rbd < 10:
    # la balle rebondit sur les bords
    if abs(x + vx) >= d - rayon: 
        vx = -vx
        rbd += 1
    if abs(y + vy) >= d - rayon:
        vy = -vy
        rbd += 1
    # ou avance 
    x += vx
    y += vy
    # on dessine la balle 
    cercle = plt.Circle((x, y), radius = rayon, fill = False) # dessine une série de ronds d'aire 1600 
    ax.add_artist(cercle) 
    # on s'arrête entre chaque tracé de cercle
    plt.pause(0.01)
# on montre le tout
plt.show()


