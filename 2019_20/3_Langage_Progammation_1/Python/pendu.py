import random

#urlretrieve('https://www.gutenberg.org/files/2650/2650-0.txt', 'swann.txt')
#fic = open('swann.txt', 'r').read()
#texte = re.sub('[^A-ZÉÈÀÙÊÔÂÜÛÇÏÎa-zéèàùêôâüûçïî]+', ' ', fic)

fic = open('mots_frequents.txt', 'r').read()
mots = [m for m in fic.split() if len(m) > 4]

mot = random.choice(mots)
liste_mot = mot.split()
long = len(mot)
nb_essais = 2*long

n = len(mot) 
mot_actuel = "_ "*n
liste_lettres = ["_ "]*n
choisies = []
nb_lettres = 0
essai = 0

while nb_lettres < n and essai < nb_essais:
    print(f"Mot à deviner : {mot_actuel}\n")
    print(f"Il vous reste {nb_essais - essai} essai(s)\n")
    lettre = input('Choisissez une lettre : ')
    essai += 1
    if lettre in choisies:
        print(f"{lettre} a déjà été choisie\n")
    else:
        choisies.append(lettre)
        if lettre not in mot and nb_essais - essai > 0 :
            print("Pas de chance, recommencez\n")
        else:
            for idx, car in enumerate(mot):
                if car == lettre:
                    print('Bon choix\n')
                    liste_lettres[idx] = lettre + ' '
                    nb_lettres += 1
            mot_actuel = ''.join(liste_lettres)
res = f"Gagné en {essai} essais !" if essai < nb_essais else "Perdu :("
print(f"{res}\n")
print(f"Le mot était {mot}")
