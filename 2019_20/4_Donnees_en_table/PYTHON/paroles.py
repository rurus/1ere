# IMPORTS
from urllib.request import urlretrieve
import csv

fichier_paroles_csv, _ = urlretrieve('https://raw.githubusercontent.com/Informathix/2nde/master/2019_20/Projet2_Donnees/ParoleFemmes.csv')
lecteur_paroles = csv.DictReader(open(fichier_paroles_csv, 'r', encoding='utf8'))

paroles = [dict(ligne) for ligne in lecteur_paroles]

#1 Ensemble des radios
radios = {
    ligne['channel_name'] 
    for ligne in paroles 
    if ligne['media_type'] == 'radio' 
}
#2 Ensembles des chaînes de télé
teles = {
    ligne['channel_name'] 
    for ligne in paroles 
    if ligne['media_type'] == 'tv' 
}

#3 Plus grand taux de parole radio 2017
max_taux = max(
    {
        (ligne['women_expression_rate'], ligne['channel_name']) 
        for ligne in paroles 
        if ligne['media_type'] == 'radio' and ligne['year'] == '2017'
    }
)

#4 Plus grand taux de progression de taux de paroles de femmes 
#  entre 2012 et 2019

def taux_prog(xs, canal):
    return ((xs[-1] - xs[0])/xs[0], canal)

def max_prog(media):
    return max(
    {
        taux_prog(
            [
            float(ligne['women_expression_rate'])
            for ligne in paroles
            if 2012 <= int(ligne['year']) and ligne['channel_name'] == canal
            ],
            canal
        )
        for canal in media
    }
)

################################################
#
#  Programme principal
#
##################################################

print(max_prog(teles))
print(max_prog(radios))