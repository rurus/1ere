# Jouons avec les noms et les prénoms


* Voici un fichier de 10 Mo contenant 
[800000 noms de familles](https://www.data.gouv.fr/fr/datasets/r/9ae80de2-a41e-4282-b9f8-61e6850ef449) recensés en France
en 2018.
* En voici un autre contenant 
[200000 prénoms](https://www.data.gouv.fr/fr/datasets/r/4b13bbf2-4185-4143-92d3-8ed5d990b0fa) 
recensés en France en 2018.
* Voici un fichier zippé contenant le [recensement les prénoms donnés en France depuis
  1900](https://www.insee.fr/fr/statistiques/fichier/2540004/dpt2018_csv.zip)
  classés par départements.  Il contient 3,5 millions de lignes  !! **Il ne faut
  surtout pas l'ouvrir dans un tableur !!**
  
**On  voudrait utiliser  ces fichiers  pour faire  certaines recherches,  certains
classements.  Pour cela,  il faut  bien  avoir maîtrisé  tout  ce qui  a été  vu
auparavant dans le cours !**

**Vous présenterez votre  enquête dans un notebook interactif au  format `ipynb` que
vous pourrez  ensuite mettre  sur votre  compte gitlab et  vous me  fournirez le
lien.**


## Informations sur le dernier fichier 



Le fichier départemental comporte 3 624 994 enregistrements et cinq variables décrites ci-après.

Ce fichier est trié selon les variables `SEXE`, `PREUSUEL`, `ANNAIS`, `DPT`.

* Nom : `SEXE` - intitulé : `sexe` - Type : `caractère` - Longueur : 1 - Modalité : `1` pour masculin, `2` pour féminin
* Nom : `PREUSUEL` - intitulé : `premier prénom` - Type : `caractère` - Longueur : `25`
* Nom : `ANNAIS` - intitulé : `année de naissance` - Type : `caractère` - Longueur : `4` - Modalité : 1900 à 2018, `XXXX`
* Nom : `DPT` - intitulé : `département de naissance` - Type : `caractère` - Longueur : `3` - Modalité : liste des départements, `XX`
* Nom : `NOMBRE` - intitulé : `fréquence` - Type : `numérique` - Longueur : `8`




La refonte  du processus électoral a  entrainé un nombre de  corrections dans la
base des  prénoms plus important  que les  années précédentes. En  effet, chaque
électeur  est  maintenant  inscrit  au  répertoire  électoral  unique  avec  son
état-civil officiel (celui du Répertoire national d'identification des personnes
physiques / RNIPP), des anomalies ont donc été corigées.

Le fichier des prénoms est établi à  partir des seuls bulletins de naissance des
personnes   nées  en   France  (métropole   et  départements   d’outre-mer  hors
Mayotte).  En  conséquence,  l’exhaustivité  n’est pas  garantie  sur  toute  la
période, notamment pour les années antérieures à 1946. Les utilisateurs pourront
donc  constater des  écarts  avec le  nombre annuel  des  naissances évalué  par
l'Insee. Ces écarts, importants en début de période, vont en s’amenuisant. Après
1946, ils sont peu significatifs. 

Les  informations contenues  dans le  fichier des  prénoms sont  basées sur  les
bulletins d'état-civil  transmis à  l’Insee par les  officiers d’état  civil des
communes. Ces  bulletins sont  eux-mêmes établis à  partir des  déclarations des
parents.  L'Insee ne  peut  garantir  que le  fichier  des  prénoms soit  exempt
d'omissions ou d'erreurs.

## Pour comprendre

Pour chaque prénom, il est indiqué pour chaque année de naissance (de 1900 à 2018) et chaque sexe, le nombre de personnes inscrites à l'état civil sous ce prénom. Pour le fichier « DPT2018 », la précision est apportée pour chaque département.

### Les personnes prises en compte

Le champ couvre l'ensemble des personnes nées en France hors Mayotte et enregistrées à l'état civil sur les bulletins de naissance. Les personnes nées à l'étranger sont exclues.

### Le champ des prénoms retenus

Dans les fichiers de l’état civil, en l'occurrence les bulletins de naissance, les différents prénoms sont séparés par une espace (ou blanc). Ainsi deux prénoms séparés par un tiret constituent un seul prénom composé (exemple : Anne-Laure). Le premier prénom simple ou composé figure en début de liste, et c'est celui qui sera retenu après le traitement de la protection de l'anonymat.

### Conditions portant sur les prénoms retenus

1. Sur la période allant de 1900 à 1945, le prénom a été attribué au moins 20 fois à des personnes de sexe féminin et/ou au moins 20 fois à des personnes de sexe masculin
2. Sur la période allant de 1946 à 2018, le prénom a été attribué au moins 20 fois à des personnes de sexe féminin et/ou au moins 20 fois à des personnes de sexe masculin
3. Pour une année de naissance donnée, le prénom a été attribué au moins 3 fois à des personnes de sexe féminin ou de sexe masculin

Les effectifs des prénoms ne remplissant pas les conditions 1 et 2 sont regroupés (pour chaque sexe et chaque année de naissance) dans un enregistrement dont le champ prénom (PREUSUEL) prend la valeur «_PRENOMS_RARES_». Les effectifs des prénoms remplissant la condition 2 mais pas la condition 3 sont regroupés (pour chaque sexe et chaque prénom) dans un enregistrement dont le champ année de naissance (ANNAIS) prend la valeur «XXXX».

### Précision pour le département de naissance

Sur toute la période, le département de naissance (variable DPT) est celui existant au moment de la naissance. Ainsi une personne née à Issy-les-Moulineaux en 1949 sera codée en 75 (Seine), et une personne née en 1971 à Issy-les-Moulineaux sera codée en 92 (Hauts-de-Seine).

En effet, de par la loi n° 64-707 du 10/07/1964, il y a eu création des départements 75 (Paris), 78 (Yvelines), 91 (Essonne), 92 (Hauts-de-Seine), 93 (Seine-Saint-Denis), 94 (Val-de-Marne) et 95 (Val-d'Oise), à partir des anciens départements 75 (Seine) et 78 (Seine-et-Oise), avec une date d'effet dans le fichier au 1er janvier 1968.

*Deux regroupements de codes ont été faits pour la variable DPT* :

* le code DPT=20 est utilisé pour les naissances ayant eu lieu dans les deux départements 2A (Corse-du-Sud) et 2B (Haute-Corse) ;
* le code DPT=97 est utilisé pour les naissances ayant eu lieu en Guadeloupe, en MArtinique, en Guyane et à La Réunion.

### Décrets et lois relatifs aux départements métropolitains

* Décret du 18/01/1955 : la Seine-Inférieure devient Seine-Maritime (76)

* Décret du 09/03/1957 : la Loire-Inférieure devient Loire-Atlantique (44)

* Loi n° 64-707 du 10/07/1964 : création des départements 75 (Paris), 78 (Yvelines), 91 (Essonne), 92 (Hauts-de-Seine), 93 (Seine-Saint-Denis), 94 (Val-de-Marne) et 95 (Val-d'Oise)

* Décret du 10/10/1969 : les Basses-Pyrénées deviennent Pyrénées-Atlantiques (64)

* Décret du 13/04/1970 : les Basses-Alpes deviennent Alpes-de-Haute-Provence (04)

* Loi n° 75-356 du 15/05/1975 : création des départements 2A (Corse-du-Sud) et 2B (Haute-Corse)

* Décret du 27/02/1990 : les Côtes-du-Nord deviennent Côtes-d'Armor (22)

### Éléments de contexte sur le choix des prénoms

Il n'existe pas  de liste de prénoms autorisés. Les  prénoms connus étrangers ou
certains diminutifs peuvent ainsi être  choisis. En conséquence, on peut trouver
par exemple des prénoms à une seule  lettre dans le fichier des prénoms. Ce sont
effectivement  les  parents qui  choisissent  librement  le  ou les  prénoms  de
l’enfant  lors de  la  déclaration  de naissance.  En  particulier depuis  1993,
l’officier  d’état  civil  ne  peut  plus  refuser  le  prénom  choisi  par  les
parents. Il peut toutefois avertir le procureur de la République s’il estime que
le prénom nuit à l’intérêt de l’enfant (exemple : prénom ridicule ou grossier) ;
ou  que le  prénom méconnaît  le droit  d’un tiers  à voir  protéger son  nom de
famille (exemple  : un  parent ne peut  choisir comme prénom  le nom  de famille
d’une autre  personne dont l’usage  constituerait une usurpation).  Le procureur
peut  ensuite saisir  le  juge  aux affaires  familiales  qui  peut demander  la
suppression du prénom sur  les registres de l’état civil. Dans  ce cas le prénom
serait également supprimé du fichier des prénoms. 

L'alphabet  utilisé pour  l’écriture  des prénoms  doit être  celui  qui sert  à
l'écriture du  français. Les caractères  alphabétiques qui ne sont  pas utilisés
dans la langue française ne sont donc pas  autorisés (par exemple le « ñ » ou le
« ș »  ). Ainsi, par exemple,  le prénom d'origine roumaine  "Rareș" apparaît en
tant que "RARES" dans le fichier des prénoms. 


