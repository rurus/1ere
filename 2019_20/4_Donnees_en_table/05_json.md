# JSON

## Présentation du site officiel 

`JSON` (*JavaScript  Object Notation – Notation  Objet issue de JavaScript*)  est un
format léger  d'échange de données. Il  est facile à  lire ou à écrire  pour des
humains. Il est  aisément analysable ou générable par des  machines. Il est basé
sur  un  sous-ensemble  du   langage  de  programmation  JavaScript  (JavaScript
Programming Language, Standard  ECMA-262 3rd Edition - December  1999). `JSON` est
un format texte  complètement indépendant de tout langage,  mais les conventions
qu'il  utilise  seront  familières  à  tout  programmeur  habitué  aux  langages
descendant du  `C`, comme  par exemple  : `C` lui-même,  `C++`, `C#`,  `Java`, `JavaScript`,
`Perl`, `Python` et bien d'autres. Ces  propriétés font de `JSON` un langage d'échange
de données idéal. 

`JSON` se base sur deux structures:

* Une collection de couples nom/valeur. Divers langages la réifient par un objet, un enregistrement, une structure, un dictionnaire, une table de hachage, une liste typée ou un tableau associatif.
* Une liste de valeurs ordonnées. La plupart des langages la réifient par un tableau, un vecteur, une liste ou une suite.

Ces structures de  données sont universelles. Pratiquement tous  les langages de
programmation  modernes les  proposent  sous  une forme  ou  une  autre. Il  est
raisonnable  qu'un  format  de  données interchangeable  avec  des  langages  de
programmation se base aussi sur ces structures. 

En `JSON`, elles prennent les formes suivantes:

Un objet, qui est un ensemble de couples nom/valeur non ordonnés. 
Un  objet commence  par  `{`(accolade  gauche) et  se  termine par  `}`(accolade
droite). 
Chaque nom est suivi de `:`(deux-points) et les couples nom/valeur sont séparés par `,`(virgule).


## Exploration des musées londoniens



Nous voulons planifier une visite de musées londoniens. Nous savons que nous
devrons commencer par la Wellcome Collection. Nous choisirons ensuite les musées
selon leur éloignememnt de ce point de  départ. **Nous voulons donc créer la liste
des  musées   londoniens  classés   selon  leur   éloignement  de   la  Wellcome
Collection**.


Nous allons pour cela utiliser la base de données cartographique 
[Open Street Map](https://www.openstreetmap.org/about) 
et plus  particulièrement [Overpass  Turbo](https://overpass-turbo.eu/) (étudier
sa [documentation](https://wiki.openstreetmap.org/wiki/FR:Overpass_turbo)).



1. Pour cela, nous allons dans  un premier temps récupérer l'ensemble des musées
   londoniens  grâce à  une  requête effectuée  sur  `overpassTurbo`.
   
   **Quelle requete doit-on écrire ? Comment faire ?**
2. Sur la partie droite de l'écran, vous voyez apparaître les musées représentés
   par des cercles  sur une carte. Si vous cliquez  sur l'onglet `Données`, vous
   voyez apparaître un texte qui débute ainsi :

```json
{
  "version": 0.6,
  "generator": "Overpass API 0.7.55.7 8b86ff77",
  "osm3s": {
    "timestamp_osm_base": "2020-01-05T09:57:02Z",
    "timestamp_areas_base": "2020-01-05T09:10:02Z",
    "copyright": "The data included in this document is from www.openstreetmap.org. The data is made available under ODbL."
  },
  "elements": [
    {
      "type": "node",
      "id": 19085022,
      "lat": 51.669645,
      "lon": -0.0682606,
      "tags": {
        "name": "Forty Hall Museum",
        "toilets:wheelchair": "yes",
        "tourism": "museum",
        "wheelchair": "yes",
        "wikidata": "Q5472972"
      }
    },
    {
      "type": "node",
      "id": 27408513,
      "lat": 51.5108862,
      "lon": -0.307107,
      "tags": {
        "name": "Pitzhanger Manor House and Gallery",
        "tourism": "museum",
        "website": "https://www.pitzhanger.org.uk/",
        "wikidata": "Q2097388"
      }
    },
    {
      "type": "node",
      "id": 29269886,
      "lat": 51.5228224,
      "lon": -0.1547768,
      "tags": {
        "addr:city": "London",
        "addr:housenumber": "8-10",
        "addr:postcode": "NW1 5LR",
        "addr:street": "Marylebone Road",
        "email": "guest.experience@madame-tussauds.com",
        "fhrs:confidence_management": "10",
        "fhrs:hygiene": "5",
        "fhrs:id": "428012",
        "fhrs:local_authority_id": "01066/4000/0/000",
        "fhrs:rating": "4",
        "fhrs:structural": "5",
        "name": "Madame Tussauds",
        "name:ru": "Мадам Тюссо",
        "name:zh": "杜莎夫人蜡像馆",
        "opening_hours": "Mo-Fr 09:30-17:30; Sa-Su 09:00-18:00",
        "phone": "+44 871 894 3000",
        "source:addr": "FSA Food Hygiene Ratings Database",
        "source:postcode": "FSA Food Hygiene Ratings Database",
        "toilets:wheelchair": "yes",
        "tourism": "museum",
        "website": "https://www.madametussauds.co.uk/london",
        "wheelchair": "yes",
        "wheelchair:description": "Bis auf die Bustour am Ende kann man sich alles ansehen. Und wenn man Hilfe braucht um sich mit einer Figur fotografieren zu lassen sind die Mitarbeiter zur Stelle.",
        "wikidata": "Q186309",
        "wikipedia": "en:Madame Tussauds London"
      }
    },
    {
      "type": "node",
      "id": 37124475,
      "lat": 51.36511,
      "lon": -0.1668055,
      "tags": {
        "description": "The water tower is a very unusual early 18th century garden building. As the name suggests, this contained a water-powered pump which supplied water to Carshalton House and the fountains in its garden.",
        "flickr": "https://www.flickr.com/photos/rogersg/3667834465/",
        "name": "Carshalton Water Tower",
        "tourism": "museum",
        "website": "http://www.carshaltonwatertower.co.uk",
        "wikidata": "Q17531582"
      }
    },
    {
      "type": "node",
      "id": 261700004,
      "lat": 51.4736073,
      "lon": -0.1155806,
      "tags": {
        "name": "The Type Museum",
        "tourism": "museum",
        "wikidata": "Q7860886",
        "wikipedia": "en:Type Museum"
      }
    },
	...
```


Nous constatons que le `JSON` a bien  un format familier, à savoir 
`clé : valeur`. 

Par exemple, `"lat": 51.4736073` donne la latitude du lieu, 
`"name": "The Type Museum"` donne son nom, etc.



Nous utiliserons la 
[bibliothèque python `json`](https://docs.python.org/fr/3.7/library/json.html).

Si  vous disposez  d'un fichier  `exportOSM.json` sur  le modèle  ci-dessus dans
votre répertoire de travail, voici comment l'importer :

```python
import json
from geopy.distance import geodesic

with open("./exportOSM.json") as js:
    json_brut = json.load(js)['elements']
```

Alors :

```python
In [1]: json_brut
Out[1]: 
[{'type': 'node',
  'id': 19085022,
  'lat': 51.669645,
  'lon': -0.0682606,
  'tags': {'name': 'Forty Hall Museum',
   'toilets:wheelchair': 'yes',
   'tourism': 'museum',
   'wheelchair': 'yes',
   'wikidata': 'Q5472972'}},
 {'type': 'node',
  'id': 27408513,
  'lat': 51.5108862,
  'lon': -0.307107,
  'tags': {'name': 'Pitzhanger Manor House and Gallery',
   'tourism': 'museum',
   'website': 'https://www.pitzhanger.org.uk/',
   'wikidata': 'Q2097388'}},
   .
   .
   .
   .
   
In [2]: type(json_brut)
Out[2]: list

In [3]: type(json_brut[0])
Out[3]: dict
```


Mais attention, le module `json` ne fonctionne pas comme `csv` !

```python
In [4]: type(json_brut[0]['lat'])
Out[4]: float
```

Pour  le  calcul  des distances  sur  le  globe  en  fonction des  latitudes  et
longitudes, on pourra utiliser la fonction `geodesic` du module 
[`geopy.distances`](https://geopy.readthedocs.io/en/stable/) avec la syntaxe:

```
geodesic((lat1, long1), (lat2, long2)).km
```

**Imaginez ensuite  d'autres enquêtes  géographiques que  vous présenterez  sur un
cahier interactif `ipynb`.**
